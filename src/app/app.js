
angular.module('orbitdemo', [
  'ngRoute',
  'orbitdemo.messages',
  'orbitdemo.settings',
  'orbitdemo.segments',
  'orbitdemo-templates',
  'googlechart',
  'ui.bootstrap'
])
.config(function ($routeProvider) {
  'use strict';
  $routeProvider
    .when('/', {
      controller: 'SegmentsCtrl',
      templateUrl: '/orbitdemo/segments/segments.html'
    })
    .when('/settings', {
      controller: 'SettingsCtrl',
      templateUrl: '/orbitdemo/settings/settings.html'
    })
    .when('/messages', {
      controller: 'MessagesCtrl',
      templateUrl: '/orbitdemo/messages/messages.html'
    })
    .otherwise({
      redirectTo: '/'
    });
})
