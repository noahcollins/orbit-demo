angular
  .module('orbitdemo.settings')
  .controller('SettingsCtrl', function ($scope, $window) {
    'use strict';
    $scope.settings = {
      personName: 'Noah Collins',
      personEmail: 'me@example.com'
    };
    $scope.updateSettings = function() {
    };
  });
