angular
  .module('orbitdemo.messages')
  .controller('MessagesCtrl', function ($scope, $window) {
    'use strict';

    $scope.setSelectedMail = function (mail) {
      $scope.selectedMail = mail;
    };

    $scope.isSelected = function (mail) {
      if ($scope.selectedMail) {
        return $scope.selectedMail === mail;
      }
    };
  })

  .controller('MailListingCtrl', function ($scope) {
    'use strict';
    $scope.email = [
      {id: 1, sent: '2014-05-24T19:02:05', from:'Edward Albee', email: 'ealbee@somewhere.com', to:'noah@email.com', subject:'Another great example', body: 'Ad ogni modo, per tornare in carreggiata, qualunque sia il genere o la fortuna dell\'opera, resta fissata questa legge che la prefazione si fa per l\'ultima. E se non bastassero le prove addotte, basterebbe pensare un poco al preludio di un dramma musicale. Ivi il maestro espone o riassume i motivi principali dell\'opera, quasi li racconta ad uno ad uno al pubblico, il quale per lo più non è loro avaro di applausi d\'incoraggiamento in principio, quanto è prodigo poi di energici fischi di scoraggiamento alla fine. Ma se l\'infelice maestro non avesse già finita l\'opera, come potrebbe accennarne i motivi principali nel preludio? È dunque provato che l\'esordio si fa dopo la conclusione: il che era da dimostrare.'},
      {id: 1, sent: '2014-05-23T11:14:20', from:'Edward Albee', email: 'ealbee@somewhere.com', to:'noah@email.com', subject:'Snapshots & online backups', body: 'Tutto ciò in un ordine astratto e mai definibile. Concretamente, s\'ha il gran rimescolio prodotto dai commerci, dalle istituzioni religiose, civili e scientifiche, dalle leghe, dalle guerre, dalle paci. I frati che lontano dalla loro patria si trovano a predicare a popolazioni cui sarebbe vano rivolgere la parola in latino, i pellegrini che accorrono alla tomba degli Apostoli e ad altri Santuari, la moltitudine raccolta insieme alle fiere, i podestà che con un loro seguito vanno ad esercitare fuor di casa l\'ufficio di supremi reggitori, la folla dei giovani che trae da ogni parte alla dotta Bologna e ivi s\'affratella, son tanti fattori di ravvicinamento tra le varie parlate, le quali imparano così a conoscersi a vicenda e acquistano scambievole familiarità. E i canti che anche allora probabilmente erravano da questa a quella provincia, e i proverbi che erravan del pari, gli uni e gli altri subendo bensì nel loro vagabondare una trasformazione, ma una trasformazione imperfetta, portavano all\'opera che si veniva compiendo un contributo tutt\'altro che disprezzabile. E un contributo stragrande veniva a portarlo il latino stesso, in quanto dappertutto il volgare, nella bocca, e più assai poi sotto la penna della gente più o meno colta, tendeva a tenerglisi stretto a\' panni. Ne seguivano convenienze senza bisogno d\'accordo: a quel modo che anche oggi il milanese e il bergamasco di chi ha la familiarità coll\'italiano, si assomigliano maggiormente che il milanese e il bergamasco del popolo rozzo.'},
      {id: 2, sent: '2014-05-23T18:01:31', from:'Claudio Paganelli', email: 'cpaganelli@emailaddress.com', to:'noah@email.com', subject:'Incoraggiamento in principio', body: 'Ma perchè tutti questi fatti non hanno grandi narratori, non hanno molti documenti? Torniamo a ripeterlo, perchè il Comune non era nato, non poteva quindi fare trattati in suo nome; e se combatteva per proprio conto, lo faceva col consenso di Matilde, dalla quale tuttavia dipendeva, dalla quale non era e non voleva essere indipendente, perchè non gli conveniva. Cominciamo dal determinar bene la questione. Dicendo lingua per contrapposto ai dialetti, noi intendiamo l\'universale di fronte al particolare; l\'unità di contro alla moltiplicità; in termini più chiari, una forma di linguaggio che si adotti per gli usi del parlar colto e dello scrivere dagli abitatori di tutta una regione, rinunziando per cotali usi alla svariatezza delle proprie favelle domestiche.'},
      {id: 3, sent: '2014-05-22T16:55:07', from:'Johanna Buckley', email: 'j.buckley@example.org', to:'noah@email.com', subject:'Evaluating new typefaces', body: 'Raccogliere ascoltatrici e ascoltatori devoti, quanti amano genialità di studi, vigoria di pensieri, pittrice eleganza nel dire, e invitare g\'lingegni più colti, perchè ognun di essi nelle spirituali adunanze, colorisca, secondo un ordine determinato, una parte del gran quadro della Vita Italiana nei varii secoli; parve assunto degno di quelle tradizioni di gentilezza onde Firenze si onora, e occasione bene augurata per procurare che i più valenti, mossi da un solo pensiero, illustrino le pagine gloriose della storia nostra civile.'},
      {id: 4, sent: '2014-05-22T16:13:44', from:'Malik Bendjelloul', email: 'malik397@hotmail.com', to:'noah@email.com', subject:'Roadmap meeting', body: 'È chiaro infatti che, le leggi naturali non avendo mai subito alcun mutamento, gli autori della più remota ed incredibile antichità debbano aver avuto le stesse passioni e sofferti gli stessi bisogni che questi moderni. Intendo rispetto alle relazioni col pubblico, e non al contenuto delle opere. Non so se come tutte le invenzioni anche questa ci venga dalla China.'},
      {id: 5, sent: '2014-05-19T15:20:04', from:'Jack George', email: 'jgeorge@somewhere.com', to:'noah@email.com', subject:'Saturday will be the best day', body: 'Tali sono le condizioni in cui noi ci possiamo immaginare Firenze, prima che il Comune nascesse. E tanto è vero che i Fiorentini avevano già di fatto una indipendenza reale, prima che avessero una indipendenza legale, che noi troviamo nel 1107, nel 1110, nel 1113 tre guerre da essi combattute contro i vicini castelli di Monte Orlando, di Prato, di Monte Cascioli, vincendo i Cadolingi e gli Alberti, conti allora potentissimi. E queste guerre furono condotte nell\'interesse commerciale delle Città, contro il feudalismo, il che ci riconferma nella opinione, che le forze crescenti della cittadinanza venivano dal commercio e dall\'industria, e che gli artigiani dovevano formare la parte principalissima di quella cittadinanza, se tutto si faceva a loro vantaggio.'},
      {id: 6, sent: '2014-05-20T13:22:37', from:'Candice Dale', email: 'candicedale@example.com', to:'noah@email.com', subject:'Gli albori della vita Italiana', body: 'La guerra civile non era mai apparsa in Milano così feroce, col suo sinistro codazzo di stragi, di vendette e di devastazioni. Fino dai primi giorni, il furore fu tale che non si dette quartiere. I popolani dovevano essere certamente dieci volte più numerosi dei nobili; ma questi avevano per sè le armature complete, i cavalli in assetto di guerra, le feritoie dei loro palazzi di pietra, la più profonda cognizione degli ordini militari. Sicchè, malgrado la sproporzione numerica, i primi combattimenti non erano riusciti favorevoli all\'insurrezione; la quale probabilmente sarebbe stata repressa e domata, se un uomo, uscito da tutt\'altre schiere, non le avesse dato il potente aiuto del suo valore e della sua virtù.'},
      {id: 7, sent: '2014-05-19T15:12:49', from:'Seneca Aranof', email: 'saranof@domain.co.uk', to:'noah@email.com', subject:'Week three sprint', body: 'Bisogna quindi ricorrere ai documenti; ma i documenti fiorentini che noi abbiamo, cominciano quando già il comune esisteva da un pezzo. È naturale che il Comune non potesse fare dei trattati, delle leggi prima di cominciare ad esistere. Abbiamo quindi bisogno d\'aiutarci colla storia generale del tempo, coi documenti posteriori, o di altri luoghi vicini; di interpretare delle frasi; fare delle indagini, per potere, retrocedendo con la induzione, cercare la spiegazione degli avvenimenti anteriori. E così è che a voler fare davvero una buona conferenza sulle origini di Firenze, bisognerebbe farla estremamente noiosa.'},
      {id: 8, sent: '2014-05-18T14:02:50', from:'Erika Young', email: 'erika.young@domain.net', to:'noah@email.com', subject:'This is good news', body: 'La consuetudine italica del far precedere ad ogni più piccola cosa una storia completa e un profluvio di erudizione, somiglia molto al morbo della prefazione. È sempre un preambolo che si volge bensì alla crassa ignoranza del lettore e non alla sua supposta simpatia, come accade per lo più nella prefazione veramente detta; ma come preambolo deve esser messo cogli altri. Ed anch\'io per non esser meno buono italiano e meno felice proemiatore, dovrei seguire questa bella tradizione di erudita seccatura ed infliggervi il supplizio della storia e della preistoria della prefazione. Ma tanta è la cortesia che mi avete dimostrato, e per la quale vi sono gratissimo, che sento l\'obbligo di essere umano e vi risparmio la solita risalita della corrente dei secoli, la solita Bibbia e i Fenici e gli Egizi.'},
      {id: 9, sent: '2014-05-21T15:53:15', from:'Chiwetel Ejiofor', email: 'chiwetel@ejiofor.me', to:'noah@email.com', subject:'Fenici e gli Egizi', body: 'Some email messages are quite short. One or two sentences.'},
      {id: 10, sent: '2014-05-18T11:10:56', from:'Andrew Edison', email: 'andrew@edward.com', to:'noah@email.com', subject:'New paint colors', body: 'Firenze negli Orti neoplatonici, ai rezzi delle ville suburbane, nelle botteghe degli speziali, e poi nelle accademie e nei dotti ritrovi, ebbe in altri tempi il primato delle letterarie adunanze. Noi vorremmo che ora potesse modestamente dar l\'esempio di eletti convegni, in cui l\'ascoltare fosse studio e ricreazione dell animo.'},
      {id: 10, sent: '2014-05-20T12:40:26', from:'Andrew Edison', email: 'andrew@edward.com', to:'noah@email.com', subject:'Three possible approaches', body: 'Ma questi linguaggi diversificano tra di loro. O come mai, se sgorgano da una stessa sorgente? Trasportato fuori di Roma, il latino dovette sonare alquanto differente a seconda che se lo appropriavano popolazioni avvezze ad una favella o ad un\'altra: a quel modo che suona diverso l\'italiano, nonchè in bocca francese, inglese, tedesca, in quella dei nativi di ogni nostra città, di ogni nostro villaggio. La continuità dell\'azione romana, la forte unità, ed i mille contatti, attenuarono per un certo tempo gli effetti di questa condizione di cose, e poterono anche dar luogo a una convenienza maggiore e più durevole assai di ciò che a prima giunta si penserebbe; ma diversità s\'ebbero e si mantennero. Orbene: queste diversità, fattesi assai maggiori una volta che l\'impero cadde in isfacelo e l\'unità fu spezzata e accresciuta dal tempo che permette alla gocciola di scavare la pietra, sono la prima causa che ha dato origine alla moltiplicità delle lingue e dei dialetti. Insieme se n\'ebbero bene anche altre; ma di fermarci a considerarle da vicino, a noi manca qui il tempo.'}
    ];
  })

  .controller('ContentCtrl', function ($scope, $rootScope, $window) {
    'use strict';
    $scope.showingReply = false;
    $scope.replyHasFocus = false;
    $scope.reply = {};

    $scope.toggleReplyForm = function () {
      $scope.showingReply = !$scope.showingReply;
      $scope.replyHasFocus = true;
      $scope.reply = {};
      $scope.reply.to = $scope.selectedMail.email;
      $scope.reply.body = '\n\n -------- \n\n' + $scope.selectedMail.body;
    };

    $scope.discardReply = function () {
      $scope.showingReply = false;
      $scope.replyHasFocus = false;
      $scope.reply = {};
    };

    $scope.sendReply = function() {
      $scope.showingReply = false;
      $rootScope.loading = true;
      $scope.replyHasFocus = false;
      $scope.reply = {};
    };

    $scope.$watch('selectedMail', function () {
      $scope.showingReply = false; // hide reply form
      $scope.reply = {}; // reset reply object

    });
  })

  .directive('focusReply', function($timeout, $parse) {
    return {
      link: function(scope, element, attrs) {
        var model = $parse(attrs.focusReply);
        scope.$watch(model, function(value) {
          console.log('value=',value);
          if(value === true) {
            $timeout(function() {
              element[0].focus();
            });
          }
        });
        element.bind('blur', function() {
          console.log('blur')
          scope.$apply(model.assign(scope, false));
        })
      }
    };
  });
