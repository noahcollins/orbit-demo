angular
  .module('orbitdemo.segments')
  .controller('SegmentsCtrl', function($scope, $window) {
    'use strict';

    $scope.tabs = [
      { title:'Activity', content:'' },
      { title:'Most Watched', content:'' },
      { title:'Most Queued', content:'' }
    ];

    var chart1 = {};
    chart1.type = "LineChart";
    chart1.cssStyle = "height:180px; width:auto;";
    chart1.data = {"cols": [
      {id: "month", label: "Month", type: "string"},
      {id: "laptop-id", label: "Series A", type: "number"},
      {id: "desktop-id", label: "Series B", type: "number"},
      {id: "server-id", label: "Series C", type: "number"}
    ], "rows": [
      {c: [{v: "Jan"},{v: 19},{v: 12},{v: 7}]},
      {c: [{v: "Feb"},{v: 15},{v: 4},{v: 11}]},
      {c: [{v: "Mar"},{v: 18},{v: 8},{v: 4}]},
      {c: [{v: "Apr"},{v: 13},{v: 8},{v: 10}]},
      {c: [{v: "May"},{v: 20},{v: 5},{v: 18}]},
      {c: [{v: "Jun"},{v: 17},{v: 8},{v: 11}]},
      {c: [{v: "Jul"},{v: 24},{v: 6},{v: 14}]}
    ]};

    chart1.options = {
        // "title": "Chart title",
        curveType: 'function',
        legend: 'none',
        "isStacked": "true",
        "fill": 20,
        "displayExactValues": true,
        "vAxis": {
          "gridlines": {"count": 3}
        }
        // "series": {
        //   0: { color: '#b03f40' },
        //   1: { color: '#b7713f' },
        //   2: { color: '#beab3f' },
        //   3: { color: '#9fc341' },
        //   4: { color: '#397cb3' }
        // }
    };

    $scope.lchart = chart1;

    var chart2 = {};
    chart2.type = "LineChart";
    chart2.cssStyle = "height:180px; width:auto;";
    chart2.data = {"cols": [
      {id: "month", label: "Month", type: "string"},
      {id: "laptop-id", label: "Series D", type: "number"},
      {id: "desktop-id", label: "Series E", type: "number"},
      {id: "server-id", label: "Series F", type: "number"}
    ], "rows": [
      {c: [{v: "Jan"},{v: 13},{v: 12},{v: 17}]},
      {c: [{v: "Feb"},{v: 7},{v: 14},{v: 9}]},
      {c: [{v: "Mar"},{v: 19},{v: 8},{v: 4}]},
      {c: [{v: "Apr"},{v: 13},{v: 9},{v: 7}]},
      {c: [{v: "May"},{v: 20},{v: 13},{v: 4}]},
      {c: [{v: "Jun"},{v: 17},{v: 16},{v: 10}]},
      {c: [{v: "Jul"},{v: 18},{v: 9},{v: 12}]}
    ]};

    chart2.options = {
        // "title": "Chart title",
        curveType: 'function',
        legend: 'none',
        "isStacked": "true",
        "fill": 20,
        "displayExactValues": true,
        "vAxis": {
          "gridlines": {"count": 3}
        }
        // "series": {
        //   0: { color: '#b03f40' },
        //   1: { color: '#b7713f' },
        //   2: { color: '#beab3f' },
        //   3: { color: '#9fc341' },
        //   4: { color: '#397cb3' }
        // }
    };

    $scope.rchart = chart2;

  });
